"""
Urls module for review app
@author: Samy Delahaye <samy.delahaye@gmail.com>
"""
from django.urls import path
from review import views

app_name = 'review'

urlpatterns = [
    path('', views.index, name='index'),
    path('review/<slug:slug>', views.review, name='review'),
]
