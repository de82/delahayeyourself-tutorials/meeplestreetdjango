"""
Models module for review app
@author: Samy Delahaye <samy.delahaye@gmail.com>
"""
from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    """
    Category model for handling different category across application
    """
    name = models.CharField(max_length=50, verbose_name='Name')
    slug = models.SlugField(verbose_name='Slug')

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name', ]
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'


class Game(models.Model):
    """
    Game model for handling general information about a game
    """
    VIDEOGAME = 'VG'
    BOARDGAME = 'BG'

    TYPE_CHOICES = (
        (VIDEOGAME, 'Videogame'),
        (BOARDGAME, 'Boardgame'),
    )

    name = models.CharField(max_length=150, verbose_name='Title')
    slug = models.SlugField(verbose_name='Slug')
    cover = models.ImageField(upload_to='cover', verbose_name='Cover')
    description = models.TextField(verbose_name='Description')
    published_at = models.DateField(verbose_name='Published at')
    type = models.CharField(max_length=2, choices=TYPE_CHOICES, verbose_name='Type',
                            default=VIDEOGAME)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['-published_at', 'name', ]
        verbose_name = 'Game'
        verbose_name_plural = 'Games'


class Review(models.Model):
    """
    Game review model that will populate our website like a blog
    """
    title = models.CharField(max_length=50, verbose_name='Name')
    slug = models.SlugField(verbose_name='Slug')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Created at')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Updated at')
    body = models.TextField(verbose_name='Body')
    thumbnail = models.ImageField(upload_to='thumbnail', verbose_name='Cover')
    author = models.ForeignKey(User, verbose_name='Author', on_delete=models.CASCADE)
    game = models.ForeignKey(Game, verbose_name='Game', on_delete=models.CASCADE)

    categories = models.ManyToManyField(Category, related_name='reviews',
                                        verbose_name='Categories')

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-updated_at', 'title', ]
        verbose_name = 'Review'
        verbose_name_plural = 'Reviews'
