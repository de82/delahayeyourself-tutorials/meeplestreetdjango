"""
Views module for review app
@author: Samy Delahaye <samy.delahaye@gmail.com>
"""
from django.shortcuts import render, get_object_or_404
from review.models import Review


def index(request):
    latest_reviews = Review.objects.all()[:3]
    return render(request, 'review/list.html', {'latest_reviews': latest_reviews})


def review(request, slug):
    review = get_object_or_404(Review, slug=slug)
    return render(request, 'review/detail.html', {'review': review})
