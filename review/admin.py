"""
Admin module for review app
@author: Samy Delahaye <samy.delahaye@gmail.com>
"""
from django.contrib import admin

from review.models import Category, Game, Review


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', )
    ordering = ('name',)
    search_fields = ('name', )
    prepopulated_fields = {"slug": ("name",)}


class GameAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'published_at', 'type', )
    ordering = ('-published_at', 'name',)
    search_fields = ('name', )
    prepopulated_fields = {"slug": ("name",)}
    list_filter = ('type', )


class ReviewAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'author', 'created_at', 'updated_at', )
    ordering = ('-updated_at', 'title',)
    search_fields = ('title', )
    list_filter = ('author', 'game', 'categories', )
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Category, CategoryAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(Review, ReviewAdmin)
