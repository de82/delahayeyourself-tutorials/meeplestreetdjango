"""
Test module for review app
@author: Samy Delahaye <samy.delahaye@gmail.com>
"""
from django.test import TestCase
from django.urls import reverse
from django.db.models.query import QuerySet
from review.models import Review


class WebsiteTestCase(TestCase):
    """
    Base testcase for website
    """

    fixtures = ['initial.json', ]

    def test_index_page(self):
        """
        Test index page
        """
        response = self.client.get(reverse('review:index'))
        self.assertContains(response, "Latests reviews")
        self.assertEqual(type(response.context['latest_reviews']), QuerySet)
        self.assertEqual(len(response.context['latest_reviews']), 3)
        self.failUnlessEqual(response.status_code, 200)
        self.assertTemplateUsed('review/list.html')

    def test_review_page(self):
        """
        Test one review page
        """
        review = Review.objects.first()
        response = self.client.get(reverse('review:review', kwargs={'slug': review.slug}))
        self.assertContains(response, review.title)
        self.assertContains(response, review.game.name)
        self.assertEqual(type(response.context['review']), Review)
        self.failUnlessEqual(response.status_code, 200)
        self.assertTemplateUsed('review/detail.html')
