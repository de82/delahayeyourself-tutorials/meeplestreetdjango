# 📣 Disclaimer

> Le repository git que vous êtes en train de visualiser n'a eu qu'une vocation pédagogique pour accompagner les modules enseignés en DUT 1A, 2A, LPRGI et 1A de cycle ingénieur.

> Son propos n'avait qu'une portée pédagogique et peut ne pas refléter l'état actuel ou encore les bonnes pratiques de la/les technologie(s) utilisée. 

## 🎯 Utilisation

Vous êtes libre de réutiliser librement le code présent dans ce repository. Prenez garde que le code est ici daté, non mis à jour et potentiellement ouvert aux failles et/ou bugs.

## 🦕 Crédit

[Samy Delahaye](https://delahayeyourself.info)


## 🪴 Descriptif du projet


### MeepleStreet - A Django powered website example

| ||
|:---:|:---:|
|![Dashboard](screenshots/001.png)|![Dashboard](screenshots/002.png)|

## How to launch dev env ?

You have to do this before opening an IDE or any code editor

### 1. Create a virtualenv

> mkdir meeplestreet_env
> virtualenv -p python3 meeplestreet_env

### 2. Activate your virtualenv

> cd meeplestreet_env
> source bin/activate

### 3. Clone the project

> git clone https://sources.delahayeyourself.info/sdelahaye/MeepleStreet.git

### 4. Install python packages for the project

> cd MeepleStreet
> pip install -r requirements.txt

### 5. Launch devserver

> python manage.py runserver

### 6. Have a cookie !

Hooray your virtualenv is up and running for the meeplestreet project !

## Purpose

This project is only for educational purpose. It's actually a basic application for generating a dummy website. 

It's in a unfinished state because we don't use:

- cache
- cbv
- managers
- prefetch
- models are damn simple (no publish state, no markdown or rich editor ..)
- etc ..

## Using

- clone this
- create a virtualenv using python3 
- install required package by using `pip install -r requirements.txt`
- run with `python manage.py runserver`

## Test

Use `python manage.py test` to run only the two simple test

## Further information

Default admin account is `admin:Licorne42*`

## Authors

* [Samy Delahaye](https://delahayeyourself.info)
